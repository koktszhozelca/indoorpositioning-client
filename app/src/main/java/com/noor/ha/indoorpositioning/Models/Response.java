package com.noor.ha.indoorpositioning.Models;

import com.google.gson.annotations.SerializedName;

public class Response<T extends Object> {
    @SerializedName("num_results")
    public int num_results;
    @SerializedName("results")
    public T[] results;

    public Response(int num_results, T[] results) {
        this.num_results = num_results;
        this.results = results;
    }

    public int getNum_results() {
        return num_results;
    }

    public T[] getResults() {
        return results;
    }
}
