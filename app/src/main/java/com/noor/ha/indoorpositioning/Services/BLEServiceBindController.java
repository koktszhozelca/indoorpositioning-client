package com.noor.ha.indoorpositioning.Services;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.google.android.material.snackbar.Snackbar;
import com.noor.ha.indoorpositioning.MainActivity;
import com.noor.ha.indoorpositioning.Models.Response;
import com.noor.ha.indoorpositioning.Models.iBeacon;
import com.noor.ha.indoorpositioning.R;
import com.noor.ha.indoorpositioning.Services.Hardwares.Permission;
import com.noor.ha.indoorpositioning.Services.Hardwares.Sensors;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.GET;


public abstract class BLEServiceBindController implements Permission.OnPermissionGranted {
    private static final String TAG = "[BLESerBindCtler]";
    public boolean isBound = false;
    private Retrofit retrofit;
    private ArrayList<iBeacon> iBeacons;
    private Activity activity;
    private boolean isPermissionGranted = false;
    private BLEService bleService;
    private Sensors ble;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (ble.isBLEEnabled()) {
                BLEService.BLEServiceBinder binder = (BLEService.BLEServiceBinder) service;
                bleService = binder.getService(ble);
                isBound = true;
                OnBLEServiceBind(bleService);
                Log.i(TAG, "BLE Service is bound: " + isBound);
                fetchiBeacons();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Error: Bluetooth isn't enabled")
                        .setMessage("Please enable Bluetooth and restart the application.")
                        .setNegativeButton("Okay", (dialog, which) -> activity.finish())
                        .show();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    public BLEServiceBindController(MainActivity activity) {
        this.activity = activity;
        ble = Sensors.getInstance(activity);
        if (!Permission.isAllGranted(activity))
            Permission.getInstance().request(activity, this);
        else isPermissionGranted = true;
        retrofit = activity.getRetrofit();
        iBeacons = new ArrayList<>();
    }

    public abstract void OnBLEServiceBind(BLEService bleService);

    @Override
    public void granted() {
        isPermissionGranted = true;
    }

    @Override
    public void rejected() {
        isPermissionGranted = false;
    }

    public void bindService() {
        Log.d(TAG, "BIND SERVICE");
        if (!isPermissionGranted) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle("Error: Permissions are not granted")
                    .setMessage("Please grant the required permissions first.")
                    .setNegativeButton("Okay", null)
                    .show();
            return;
        }
        Intent intent = new Intent(activity, BLEService.class);
        activity.bindService(intent, serviceConn, Context.BIND_AUTO_CREATE);
        Log.d(TAG, "BIND SERVICE");
    }

    public void unBindService() {
        if (!isPermissionGranted) return;
        activity.unbindService(serviceConn);
        Log.i(TAG, "BLE Service is un-bound: " + isBound);
    }

    private void fetchiBeacons() {
        iBeaconAPI iBeaconAPI = retrofit.create(iBeaconAPI.class);
        Call<Response<iBeacon>> ibeacons = iBeaconAPI.getiBeacons();
        ibeacons.enqueue(new iBeaconEnqueue());
    }

    private void toast(String message) {
        Snackbar.make(activity.findViewById(R.id.main_layout), message, Snackbar.LENGTH_SHORT).show();
    }

    public interface iBeaconAPI {
        @GET("/ibeacon")
        Call<Response<iBeacon>> getiBeacons();
    }

    private class iBeaconEnqueue implements Callback<Response<iBeacon>> {
        @Override
        public void onResponse(Call<Response<iBeacon>> call, retrofit2.Response<Response<iBeacon>> response) {
            iBeacons.clear();
            Response<iBeacon> res = response.body();
            if (res.num_results > 0) {
                for (iBeacon iBeacon : res.results) {
                    Log.d(TAG, "iBeacon" + iBeacon.toString());
                    iBeacons.add(iBeacon);
                }
            }
            Log.d(TAG, "There are " + iBeacons.size() + " ibeacons being fetched.");

            ArrayList<String> iBeaconList = new ArrayList<>();
            for (iBeacon ibeacon : iBeacons) iBeaconList.add(ibeacon.identifier);
            bleService.setiBeaconsList(iBeaconList);
        }

        @Override
        public void onFailure(Call<Response<iBeacon>> call, Throwable t) {
            toast("Failed to fetch ibeacons.");
        }
    }
}
