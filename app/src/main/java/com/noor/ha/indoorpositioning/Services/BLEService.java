package com.noor.ha.indoorpositioning.Services;

import android.app.Service;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.noor.ha.indoorpositioning.Config;
import com.noor.ha.indoorpositioning.Models.Measurement;
import com.noor.ha.indoorpositioning.Services.Hardwares.Sensors;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

public class BLEService extends Service {
    public enum SCAN_MODE {
        REGULAR(1000 * 10), INTENSIVE(0);

        long interval;

        SCAN_MODE(long interval) {
            this.interval = interval;
        }
    }

    public interface OnBLEScanListener {
        void onBLEScanStart();

        void onBLEScanStop();
    }

    public interface OnStorageFullListener {
        void onFull(Measurement measurement);
    }

    private static final String TAG = "[BLEService]";

    private BLEServiceBinder serviceBinder = new BLEServiceBinder();

    private OnBLEScanListener onBLEScanListener;
    private OnStorageFullListener onStorageFullListener;

    private boolean isScanning = false;
    private Sensors ble;
    private ScanCallback scanCallback = null;
    private ArrayList<String> iBeaconsList = null;

    private boolean isMeasuring = false;
    private String scanningZone;
    private Measurement measurement;

    private boolean isTesting = false;

    private TreeMap<String, Double> sampleDoc;

    public ScanCallback getScanCallback() {
        return new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                parseByDeviceName(result);
            }
        };
    }

    private void parseByDebug(ScanResult result) {
        if (iBeaconsList != null && iBeaconsList.size() > 0) {
            String deviceAddr = result.getDevice().getAddress();
            if (iBeaconsList.contains(deviceAddr)) {
                if (isMeasuring || isTesting) this.measure();
                else {
                    Log.d(TAG, "Scan results: " + result.getDevice().getAddress() + ", " + result.getRssi());
                    sampleDoc.put(deviceAddr, (double) result.getRssi());
                }
            } else Log.d(TAG, "New device " + deviceAddr);
        }
//        else Log.d(TAG, "Result " + result.toString());
    }

    private void parseByDeviceName(ScanResult result) {
        if (iBeaconsList != null && iBeaconsList.size() > 0) {
            String deviceName = result.getDevice().getName();
            if (iBeaconsList.contains(deviceName)) {
                if (isMeasuring || isTesting) this.measure();
                else {
                    Log.d(TAG, "Scan results: " + result.getDevice().getAddress() + ", " + result.getRssi());
                    sampleDoc.put(deviceName, (double) result.getRssi());
                }
            }
        }
//        else Log.d(TAG, "Result " + result.toString());
    }

    private ArrayList<Double> getRSSIFromSample() {
        ArrayList<Double> rssis = new ArrayList<>();
        for (String key : sampleDoc.keySet()) rssis.add(sampleDoc.get(key));
        return rssis;
    }

    private void measure() {
        if (measurement.size < Config.MEASUREMENT_SIZE)
            measurement.add(getRSSIFromSample());
        else {
            onStorageFullListener.onFull(measurement);
            measurement.clear();
        }
    }

    private static Timer intervalController;
    private static boolean isScheduledStopped = false;
    private SCAN_MODE scanMode = SCAN_MODE.REGULAR;

    public BLEService() {
    }

    public void setiBeaconsList(ArrayList<String> iBeaconsList) {
        this.sampleDoc = new TreeMap<>();
        this.iBeaconsList = iBeaconsList;
        for (String identifier : this.iBeaconsList) this.sampleDoc.put(identifier, 0.0);
        Log.d(TAG, "List is set " + this.iBeaconsList);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }

    public class BLEServiceBinder extends Binder {
        BLEService getService(Sensors ble) {
            BLEService.this.ble = ble;
            return BLEService.this;
        }
    }

    private void intervalControl() {
        Log.i(TAG, "Interval control start");
        intervalController = new Timer();
        intervalController.schedule(new TimerTask() {
            @Override
            public void run() {
                stopScan(true);
                Log.i(TAG, "Interval control stop scanning, isScheduledStopped: " + isScheduledStopped);
                delayStart(scanMode.interval);
            }
        }, 1000 * 10);
    }

    private void delayStart(long delay) {
        Log.i(TAG, "Delay start");
        intervalController = new Timer();
        intervalController.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.i(TAG, "Delay start run, isScanning: " + isScanning + ", isScheduledStopped: " + isScheduledStopped);
                if (isScanning && isScheduledStopped) {
                    Log.i(TAG, "Delay start before scanning, isScheduledStopped: " + isScheduledStopped);
                    startScan(true);
                }
            }
        }, delay);
    }

    private void startScan(boolean isPerformedByScheduler) {
        if (!isPerformedByScheduler && isScanning) return;
        scanCallback = getScanCallback();
        isScanning = true;
        Log.i(TAG, "Scan start");
        ble.scan(scanCallback);
        if (onBLEScanListener != null) onBLEScanListener.onBLEScanStart();
        if (isPerformedByScheduler) isScheduledStopped = false;
        intervalControl();
    }

    private void stopScan(boolean isPerformedByScheduler) {
        ble.stopScan(scanCallback);
        if (onBLEScanListener != null) onBLEScanListener.onBLEScanStop();
        if (isPerformedByScheduler) isScheduledStopped = true;
        else {
            isScheduledStopped = false;
            isScanning = false;
        }
        intervalController.cancel();
        Log.i(TAG, "Scan stop");
    }

    //Services here
    public void setScanningZone(String zoneKey) {
        this.scanningZone = zoneKey;
        Log.d(TAG, "BLE Zone is updated " + zoneKey);
    }

    public void createMeasurement() {
        this.measurement = new Measurement(this.scanningZone);
    }

    public boolean isBLEEnabled() {
        return ble.isBLEEnabled();
    }

    public void setOnBLEScanListener(OnBLEScanListener listener) {
        this.onBLEScanListener = listener;
    }

    public void setOnStorageFullListener(OnStorageFullListener listener) {
        this.onStorageFullListener = listener;
    }

    public void startMeasure() {
        this.isMeasuring = true;
    }

    public void stopMeasure() {
        this.isMeasuring = false;
    }

    public boolean isMeasuring() {
        return this.isMeasuring;
    }

    public void startTesting() {
        this.isTesting = true;
    }

    public void stopTesting() {
        this.isTesting = false;
    }

    public boolean isTesting() {
        return this.isTesting;
    }

    public void setScanMode(SCAN_MODE nextScanMode) {
        this.scanMode = nextScanMode;
    }

    public void startScan() {
        startScan(false);
    }

    public void stopScan() {
        stopScan(false);
    }
}
