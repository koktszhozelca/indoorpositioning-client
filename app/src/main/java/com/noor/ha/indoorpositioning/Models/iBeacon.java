package com.noor.ha.indoorpositioning.Models;

import com.google.gson.annotations.SerializedName;

public class iBeacon extends Model {
    @SerializedName("identifier")
    public String identifier;
    @SerializedName("x")
    float x;
    @SerializedName("y")
    float y;

    public iBeacon(String identifier, float x, float y, String __key__, long __ctime__, long __mtime__) {
        super(__key__, __ctime__, __mtime__);
        this.identifier = identifier;
        this.x = x;
        this.y = y;
    }

    public String getIdentifier() {
        return identifier;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    @Override
    public String toString() {
        return "iBeacon{" +
                "identifier='" + identifier + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", __key__='" + __key__ + '\'' +
                ", __ctime__=" + __ctime__ +
                ", __mtime__=" + __mtime__ +
                '}';
    }
}
