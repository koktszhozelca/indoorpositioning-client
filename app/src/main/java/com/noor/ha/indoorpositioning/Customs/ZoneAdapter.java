package com.noor.ha.indoorpositioning.Customs;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.noor.ha.indoorpositioning.Models.Zone;

import java.util.ArrayList;

public class ZoneAdapter extends ArrayAdapter<Zone> {

    private Context context;
    private ArrayList<Zone> zones;

    public ZoneAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Zone> zones) {
        super(context, resource, zones);
        this.context = context;
        this.zones = zones;
    }

    @Override
    public int getCount() {
        return zones.size();
    }

    @Override
    public Zone getItem(int position) {
        return zones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(zones.get(position).getDisplay(false));
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(zones.get(position).getDisplay(true));
        return label;
    }
}
