package com.noor.ha.indoorpositioning.Services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.noor.ha.indoorpositioning.Config;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;

public class HeartbeatService extends Service implements Callback<Response<String>> {
    private static final String TAG = "[HeartbeatSrv]";

    public class HeartBeatServiceBinder extends Binder {
        HeartbeatService getService(Retrofit retrofit) {
            HeartbeatService.this.retrofit = retrofit;
            return HeartbeatService.this;
        }
    }

    public interface HeartbeatAPI {
        @GET("/heartbeat")
        Call<Response<String>> getHeartbeat();
    }

    public interface OnServerStatusChangedListener {
        void onOnline();

        void onOffline();
    }

    private HeartBeatServiceBinder heartBeatServiceBinder = new HeartBeatServiceBinder();
    private Retrofit retrofit;
    private boolean isServerOnline = false;
    private OnServerStatusChangedListener listener;
    private Timer timer;

    public HeartbeatService() {
        timer = new Timer();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "ON BIND");
        return heartBeatServiceBinder;
    }

    private void monitor() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                HeartbeatAPI heartbeatAPI = retrofit.create(HeartbeatAPI.class);
                Call<Response<String>> heartbeat = heartbeatAPI.getHeartbeat();
                heartbeat.enqueue(HeartbeatService.this);
            }
        }, Config.HEARTBEAT_INTERVAL);
    }

    public void start() {
        Log.d(TAG, "Start monitoring");
        this.monitor();
    }

    public void stop() {
        Log.d(TAG, "Stop monitoring");
    }

    public boolean isServerOnline() {
        return isServerOnline;
    }

    public void setListener(OnServerStatusChangedListener listener) {
        this.listener = listener;
    }

    @Override
    public void onResponse(Call<Response<String>> call, Response<Response<String>> response) {
        isServerOnline = response.code() == 200;
        if (listener != null) listener.onOnline();
        monitor();
    }

    @Override
    public void onFailure(Call<Response<String>> call, Throwable t) {
        isServerOnline = false;
        if (listener != null) listener.onOffline();
        monitor();
    }
}
