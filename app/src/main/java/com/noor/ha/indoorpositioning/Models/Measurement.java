package com.noor.ha.indoorpositioning.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Measurement {
    @SerializedName("zoneKey")
    public String zoneKey;
    @SerializedName("rssis")
    public ArrayList<ArrayList<Double>> rssis;
    @SerializedName("size")
    public int size = 0;

    public Measurement(String zoneKey) {
        this.zoneKey = zoneKey;
        this.rssis = new ArrayList<>();
    }

    public Measurement(String zoneKey, ArrayList<ArrayList<Double>> rssis) {
        this.zoneKey = zoneKey;
        this.rssis = rssis;
    }

    public void add(ArrayList<Double> rssi) {
        this.rssis.add(rssi);
        this.size++;
    }

    public void clear() {
        this.size = 0;
        this.rssis.clear();
    }
}
