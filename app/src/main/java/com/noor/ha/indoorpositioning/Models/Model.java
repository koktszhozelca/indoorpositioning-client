package com.noor.ha.indoorpositioning.Models;

import com.google.gson.annotations.SerializedName;

public class Model {
    @SerializedName("__key__")
    public String __key__;
    @SerializedName("__ctime__")
    public long __ctime__;
    @SerializedName("__mtime__")
    public long __mtime__;

    public Model(String __key__, long __ctime__, long __mtime__) {
        this.__key__ = __key__;
        this.__ctime__ = __ctime__;
        this.__mtime__ = __mtime__;
    }

    @Override
    public String toString() {
        return "Model{" +
                "__key__='" + __key__ + '\'' +
                ", __ctime__=" + __ctime__ +
                ", __mtime__=" + __mtime__ +
                '}';
    }
}
