package com.noor.ha.indoorpositioning.Services;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.noor.ha.indoorpositioning.MainActivity;

import retrofit2.Retrofit;

public abstract class HeartbeatServiceBindController {
    private static final String TAG = "[HeartbeatCtrl]";

    public abstract void OnHeartBeatServiceBound(HeartbeatService hbService);

    public abstract HeartbeatService.OnServerStatusChangedListener setOnServerStatusChangedListener();

    private boolean isBound = false;
    private Activity activity;
    private Retrofit retrofit;
    private HeartbeatService hbService;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "On HB Srv connected");
            HeartbeatService.HeartBeatServiceBinder binder = (HeartbeatService.HeartBeatServiceBinder) service;
            hbService = binder.getService(retrofit);
            hbService.setListener(setOnServerStatusChangedListener());
            OnHeartBeatServiceBound(hbService);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "HB Src discounnted");
            isBound = false;
        }
    };

    public HeartbeatServiceBindController(MainActivity activity) {
        this.activity = activity;
        this.retrofit = activity.getRetrofit();
    }

    public void bindService() {
        Intent intent = new Intent(activity, HeartbeatService.class);
        activity.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        Log.d(TAG, "Bind service");
    }

    public void unBindService() {
        activity.unbindService(serviceConnection);
        Log.d(TAG, "unBind service");
    }
}
