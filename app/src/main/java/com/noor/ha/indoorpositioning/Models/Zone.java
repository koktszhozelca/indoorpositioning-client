package com.noor.ha.indoorpositioning.Models;

import com.google.gson.annotations.SerializedName;

public class Zone extends Model {
    @SerializedName("label")
    public String label;
    @SerializedName("description")
    public String description;


    public Zone(String label, String __key__, long __ctime__, long __mtime__) {
        super(__key__, __ctime__, __mtime__);
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public String getDisplay(boolean detail) {
        String display = label;
        if (detail && description.length() > 0)
            display += ", " + description;
        return display;
    }

    @Override
    public String toString() {
        return "Zone{" +
                "label='" + label + '\'' +
                ", description='" + description + '\'' +
                ", __key__='" + __key__ + '\'' +
                ", __ctime__=" + __ctime__ +
                ", __mtime__=" + __mtime__ +
                '}';
    }
}
