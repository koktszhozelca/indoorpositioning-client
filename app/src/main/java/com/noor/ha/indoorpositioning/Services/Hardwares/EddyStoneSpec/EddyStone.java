package com.noor.ha.indoorpositioning.Services.Hardwares.EddyStoneSpec;

import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.os.ParcelUuid;

import java.util.ArrayList;

public class EddyStone {
    public static final String UUID = "0000fe9a-0000-1000-8000-00805f9b34fb";

    public static ArrayList<ScanFilter> filters = new ArrayList<ScanFilter>() {{
        new ScanFilter.Builder()
                .setServiceUuid(ParcelUuid.fromString(UUID)).build();
    }};

    public static ScanSettings settings =
            new ScanSettings.Builder()
//                    .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER | ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
}
