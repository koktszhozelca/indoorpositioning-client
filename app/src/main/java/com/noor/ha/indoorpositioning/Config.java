package com.noor.ha.indoorpositioning;

public class Config {
    // Android Studio debug host ip address is 10.0.2.2.

    public static String IP_ADDRESS = "192.168.0.192";
    public static String PORT = "9300";
    public static int MEASUREMENT_SIZE = 20;
    public static int HEARTBEAT_INTERVAL = 1000 * 3; // 1000 ms = 1 second, 1000 * 5 = 5 seconds.

    public static void setServerConnection(String ipAddr, String port) {
        IP_ADDRESS = ipAddr;
        PORT = port;
    }

    public static void setMeasurementSize(int size) {
        MEASUREMENT_SIZE = size;
    }
}
