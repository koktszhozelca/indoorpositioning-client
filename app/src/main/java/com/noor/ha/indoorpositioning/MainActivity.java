package com.noor.ha.indoorpositioning;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.noor.ha.indoorpositioning.Customs.ZoneAdapter;
import com.noor.ha.indoorpositioning.Models.Measurement;
import com.noor.ha.indoorpositioning.Models.Query;
import com.noor.ha.indoorpositioning.Models.Response;
import com.noor.ha.indoorpositioning.Models.Zone;
import com.noor.ha.indoorpositioning.Services.BLEService;
import com.noor.ha.indoorpositioning.Services.BLEServiceBindController;
import com.noor.ha.indoorpositioning.Services.HeartbeatService;
import com.noor.ha.indoorpositioning.Services.HeartbeatServiceBindController;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, BLEService.OnStorageFullListener {
    public static String TAG = "[MAIN]";

    public interface ZoneAPI {
        @GET("/zone")
        Call<Response<Zone>> getZones();
    }

    public interface MeasureAPI {
        @POST("/measure/batch")
        Call<Measurement> pushMeasurements(@Body Measurement measurement);
    }

    public interface QueryAPI {
        @POST("/query")
        Call<Query> query(@Body Measurement measurement);
    }

    public enum MODE {
        MEASUREMENT, FINGERPRINTING, TRIANGULATION
    }


    private Retrofit retrofit;
    private Spinner selZone;
    private Button btnMeasure, btnFingerprintTest, btnSave, btnTriTest;
    private ImageButton btnRefresh;
    private EditText txtServerAddress;
    private ImageView imgServerStatus;
    private TextView txtLocation;

    private BLEService bleService;
    private BLEServiceBindController bleServiceBindController;

    private HeartbeatService hbService;
    private HeartbeatServiceBindController hbServiceBindController;

    private boolean lastServerStatus = false;
    private MODE mode = MODE.MEASUREMENT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        uiSetup();
        apiSetup();
        serviceSetup();
        onSetup();
    }

    private void uiSetup() {
        selZone = findViewById(R.id.selZone);
        txtServerAddress = findViewById(R.id.txtServerAddress);

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        btnRefresh = findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(this);

        btnMeasure = findViewById(R.id.btnMeasure);
        btnMeasure.setOnClickListener(this);

        btnFingerprintTest = findViewById(R.id.btnFingerprintTest);
        btnFingerprintTest.setOnClickListener(this);

        btnTriTest = findViewById(R.id.btnTriTest);
        btnTriTest.setOnClickListener(this);

        imgServerStatus = findViewById(R.id.imgServerStatus);
        imgServerStatus.setImageDrawable(getDrawable(R.drawable.wifi_offline));

        txtLocation = findViewById(R.id.txtLocation);
    }

    private void apiSetup() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://" + Config.IP_ADDRESS + ":" + Config.PORT + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    private void serviceSetup() {
        bleServiceBindController = new BLEServiceBindController(MainActivity.this) {
            @Override
            public void OnBLEServiceBind(BLEService bleService) {
                MainActivity.this.bleService = bleService;
                MainActivity.this.bleService.setScanMode(BLEService.SCAN_MODE.INTENSIVE);
                MainActivity.this.bleService.startScan();
                MainActivity.this.bleService.setOnStorageFullListener(MainActivity.this);
                MainActivity.this.enableBLEButtons();
            }
        };

        hbServiceBindController = new HeartbeatServiceBindController(MainActivity.this) {
            @Override
            public void OnHeartBeatServiceBound(HeartbeatService hbService) {
                MainActivity.this.hbService = hbService;
                MainActivity.this.hbService.start();
            }

            @Override
            public HeartbeatService.OnServerStatusChangedListener setOnServerStatusChangedListener() {
                return new HeartbeatService.OnServerStatusChangedListener() {
                    @Override
                    public void onOnline() {
                        serverOnline();
                    }

                    @Override
                    public void onOffline() {
                        serverOffline();
                    }
                };
            }

        };
    }

    private void serverOnline() {
        if (!lastServerStatus) {
            imgServerStatus.setImageDrawable(getDrawable(R.drawable.wifi_online));
            bleServiceBindController.bindService();


            // Dim save button
            btnSave.setEnabled(false);

            // Fetch zones
            this.fetchZones();
            lastServerStatus = true;
        }
    }

    private void serverOffline() {
        if (lastServerStatus) {
            stopMeasure();
            imgServerStatus.setImageDrawable(getDrawable(R.drawable.wifi_offline));
            bleService.stopScan();
            bleServiceBindController.unBindService();

            btnSave.setEnabled(true);
            disableBLEButtons();
            lastServerStatus = false;
        }
    }

    private void onSetup() {
        this.txtServerAddress.setText(Config.IP_ADDRESS + ":" + Config.PORT);
        disableBLEButtons();
    }

    private void fetchZones() {
        ZoneAPI zoneAPI = retrofit.create(ZoneAPI.class);
        Call<Response<Zone>> zones = zoneAPI.getZones();
        zones.enqueue(new ZoneEnqueue());
    }

    private void enableBLEButtons() {
        toggleButton(btnFingerprintTest, true);
        toggleButton(btnTriTest, true);
        toggleButton(btnMeasure, true);
    }

    private void disableBLEButtons() {
        toggleButton(btnFingerprintTest, false);
        toggleButton(btnTriTest, false);
        toggleButton(btnMeasure, false);
    }

    private void toggleButton(Button btn, boolean state) {
        btn.setEnabled(state);
        btn.setText(state ? getButtonText(btn) : "Disabled");
    }

    private String getButtonText(Button btn) {
        if (btn == btnSave) return "Save";
        else if (btn == btnMeasure) return "Measure";
        else if (btn == btnFingerprintTest) return "Fingerprinting Test";
        else if (btn == btnTriTest) return "Triangulation Test";
        return "Default label";
    }

    private void setServerConnection() {
        String serverAddr = txtServerAddress.getText().toString();
        if (serverAddr.length() < 1) toast("Please input the IP Address and Port first.");
        else {
            String[] conn = serverAddr.split(":");
            Config.setServerConnection(conn[0], conn[1]);
            apiSetup();
            toast("Server connection is updated.");
        }
    }

    private void toast(String message) {
        Snackbar.make(findViewById(R.id.main_layout), message, Snackbar.LENGTH_SHORT).show();
    }

    private void startMeasure() {
        btnMeasure.setText("Stop Measurement");
        this.bleService.createMeasurement();
        this.bleService.startMeasure();

        // Disable test buttons
        toggleButton(btnFingerprintTest, false);
        toggleButton(btnTriTest, false);
    }

    private void stopMeasure() {
        btnMeasure.setText("Start Measurement");
        this.bleService.stopMeasure();

        // Enable test buttons
        toggleButton(btnFingerprintTest, true);
        toggleButton(btnTriTest, true);
    }

    private void toggleMeasure() {
        mode = MODE.MEASUREMENT;
        if (this.bleService.isMeasuring()) stopMeasure();
        else startMeasure();
    }

    private void startTesting() {
        if (mode == MODE.FINGERPRINTING) {
            btnFingerprintTest.setText("Testing ...");
            this.bleService.createMeasurement();
            this.bleService.startTesting();
            toggleButton(btnTriTest, false);
        } else {
            btnTriTest.setText("Testing ...");
            this.bleService.createMeasurement();
            this.bleService.startTesting();
            toggleButton(btnFingerprintTest, false);
        }
        toggleButton(btnMeasure, false);
    }

    private void stopTesting() {
        btnFingerprintTest.setText("Fingerprinting Test");
        btnTriTest.setText("Triangulation Test");
        this.bleService.stopTesting();

        // Enable Measure & test buttons
        toggleButton(btnFingerprintTest, true);
        toggleButton(btnTriTest, true);
        toggleButton(btnMeasure, true);
    }

    private void toggleTest() {
        stopMeasure();
        if (this.bleService.isTesting()) stopTesting();
        else startTesting();
    }

    private void fingerprintingTest() {
        mode = MODE.FINGERPRINTING;
        toggleTest();
    }

    @Override
    public void onFull(Measurement measurement) {
        Log.d(TAG, "Measurement: " + mode);

        if (mode == MODE.MEASUREMENT) {
            MeasureAPI measureAPI = retrofit.create(MeasureAPI.class);
            Call<Measurement> res = measureAPI.pushMeasurements(measurement);
            res.enqueue(new EmptyEnqueue());
        } else if (mode == MODE.FINGERPRINTING) {
            QueryAPI queryAPI = retrofit.create(QueryAPI.class);
            Call<Query> res = queryAPI.query(measurement);
            res.enqueue(new QueryEnqueue());
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnRefresh) this.fetchZones();
        else if (v == btnSave) this.setServerConnection();
        else if (v == btnMeasure) this.toggleMeasure();
        else if (v == btnFingerprintTest) this.fingerprintingTest();
    }

    @Override
    protected void onStart() {
        super.onStart();
        hbServiceBindController.bindService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        hbServiceBindController.unBindService();
    }

    private class ZoneEnqueue implements Callback<Response<Zone>> {
        @Override
        public void onResponse(Call<Response<Zone>> call, retrofit2.Response<Response<Zone>> response) {
            ArrayList<Zone> zones = new ArrayList<>();
            Response<Zone> res = response.body();
            if (res.num_results > 0) {
                for (Zone zone : res.results) {
                    Log.d(TAG, "Zone" + zone.toString());
                    zones.add(zone);
                }
            }
            ZoneAdapter zoneAdapter = new ZoneAdapter(MainActivity.this, R.layout.selection, zones);
            selZone.setAdapter(zoneAdapter);
            selZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Zone zone = (Zone) parent.getItemAtPosition(position);
                    MainActivity.this.bleService.setScanningZone(zone.__key__);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            if (zones.size() > 0) selZone.setSelection(0);
        }

        @Override
        public void onFailure(Call<Response<Zone>> call, Throwable t) {
            toast("Failed to fetch zones.");
        }
    }

    private class QueryEnqueue implements Callback<Query> {

        @Override
        public void onResponse(Call<Query> call, retrofit2.Response<Query> response) {
            try {
                Log.d(TAG, "Query response " + response.body());
                toggleTest();
                runOnUiThread(() -> txtLocation.setText(response.body().zone.getDisplay(true)));
            } catch (Exception e) {
                runOnUiThread(() -> txtLocation.setText("500 Server error"));
            }
        }

        @Override
        public void onFailure(Call<Query> call, Throwable t) {

        }
    }


    private class EmptyEnqueue implements Callback<Measurement> {
        @Override
        public void onResponse
                (Call<Measurement> call, retrofit2.Response<Measurement> response) {

        }

        @Override
        public void onFailure(Call<Measurement> call, Throwable t) {

        }
    }
}
