package com.noor.ha.indoorpositioning.Models;

import com.google.gson.annotations.SerializedName;

public class Query {
    @SerializedName("zone")
    public Zone zone;
    @SerializedName("similarity")
    public double similarity;

    public Query(Zone zone, double similarity) {
        this.zone = zone;
        this.similarity = similarity;
    }

    @Override
    public String toString() {
        return "Query{" +
                "zone=" + zone +
                ", similarity=" + similarity +
                '}';
    }
}
